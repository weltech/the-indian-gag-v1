package tech.wel.com.theindiangag;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.senab.photoview.PhotoViewAttacher;


public class MainActivity extends AppCompatActivity {

    private InterstitialAd interstitial;

    int mPreLast;
    // Declare Variables
    private int limit = 10;
    ListView listview;
    List<ParseObject> ob;
    Dialog dialog;
    ListViewAdapter adapter;
    private List<Data_Content> worldpopulationlist = null;

    int position12 = 0;

    int sts = 0;
   AutoScrollViewPager viewPager;
    PhotoViewAttacher mAttacher;
    // Progress Dialog
    private ProgressDialog pDialog;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar actionBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        actionBar.showOverflowMenu();


        // Execute RemoteDataTask AsyncTask
        new RemoteDataTask().execute();

        // Create the interstitial.
        interstitial = new InterstitialAd(MainActivity.this);
        interstitial.setAdUnitId("ca-app-pub-7403986388841319/7447481510");
    }

    // RemoteDataTask AsyncTask
    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progressdialog);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create the array
            worldpopulationlist = new ArrayList<Data_Content>();
            try {
                // Locate the class table named "Country" in Parse.com
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Data");
                // by ascending
                query.orderByDescending("ID");
                // Set the limit of objects to show
                query.setLimit(limit);
                ob = query.find();
                for (ParseObject country : ob) {
                    // Locate images in flag column
                    ParseFile image = (ParseFile) country.getParseFile("Image");
                    Data_Content map = new Data_Content();
                    map.setFlag(image.getUrl());
                    map.setId(country.get("ID").toString());
                    map.setUploadType(country.get("type").toString());
                    if(country.getString("youtubeID") != null)
                    {
                        map.setYoutubeID(country.getString("youtubeID"));
                    }
                    else
                    {
                        map.setYoutubeID("0");
                    }

                    Format formatter = new SimpleDateFormat("ccc MMMM yyyy");
                    String s = formatter.format(country.getCreatedAt());
                    map.setDate(s);
                    worldpopulationlist.add(map);
                }
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            catch (Exception e)
            {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
                // Locate the listview in listview_main.xml


            listview = (ListView) findViewById(R.id.listview);
            adapter = new ListViewAdapter(MainActivity.this, worldpopulationlist);
            // Binds the Adapter to the ListView
            listview.setAdapter(adapter);
            // Create an OnScrollListener
            listview.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view,
                                                 int scrollState) { // TODO Auto-generated method stub

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    // TODO Auto-generated method stub
                    int lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem == totalItemCount){
                        if (mPreLast != lastItem)
                        {
                            mPreLast = lastItem;
                            new LoadMoreDataTask().execute();
                        }
                    }

                }

            });

            // Close the progressdialog
            dialog.dismiss();
        }
    }


    private class LoadMoreDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progressdialog);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create the array
            worldpopulationlist = new ArrayList<Data_Content>();
            try {
                // Locate the class table named "Country" in Parse.com
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Data");
                // Locate the column named "ranknum" in Parse.com and order list
                // by ascending
                query.orderByDescending("ID");
                query.setLimit(limit += 10);
                ob = query.find();
                for (ParseObject country : ob) {
                    // Locate images in flag column
                    ParseFile image = (ParseFile) country.getParseFile("Image");
                    Data_Content map = new Data_Content();

                    map.setFlag(image.getUrl());
                    map.setId(country.get("ID").toString());
                    map.setUploadType(country.get("type").toString());
                    if(country.getString("youtubeID") != null)
                    {
                        map.setYoutubeID(country.getString("youtubeID"));
                    }
                    else
                    {
                        map.setYoutubeID("0");
                    }

                    Format formatter = new SimpleDateFormat("ccc MMMM yyyy");
                    String s = formatter.format(country.getCreatedAt());

                    map.setDate(s);

                    worldpopulationlist.add(map);
                }
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            catch (Exception e)
            {

            }
            return null;
        }

        @Override
            protected void onPostExecute(Void result) {


            // Locate listview last item
            int position = listview.getLastVisiblePosition();
            adapter = new ListViewAdapter(MainActivity.this, worldpopulationlist);
            // Binds the Adapter to the ListView
            listview.setAdapter(adapter);
            // Show the latest retrived results on the top
            listview.setSelectionFromTop(position, 0);
            // Close the progressdialog
            dialog.dismiss();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.about:
                Intent intent = new Intent(MainActivity.this,About_me.class);
                startActivity(intent);
                return true;

            case R.id.logout:
                // Logout current user
               /* ParseUser.logOut();*/
                finish();
                Intent intent1 = new Intent(MainActivity.this,Login.class);
                startActivity(intent1);

                SharedPreferences preferences = getSharedPreferences("Login", 0);
                preferences.edit().clear().commit();

                return true;
        }
        return false;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class ListViewAdapter extends BaseAdapter {

        // Declare Variables
        private Activity activity;
        LayoutInflater inflater = null;

        private List<Data_Content> worldpopulationlist = null;
        private ArrayList<Data_Content> arraylist;
        // Declare Variables
        private int limit = 10;
        List<ParseObject> ob;
        PhotoViewAttacher mAttacher;

        public ListViewAdapter(Activity activity,
                               List<Data_Content> worldpopulationlist) {
            this.activity = activity;
            this.worldpopulationlist = worldpopulationlist;
            this.arraylist = new ArrayList<Data_Content>();
            this.arraylist.addAll(worldpopulationlist);

        }

        public class ViewHolder {
            ImageView play_button;
            SimpleDraweeView flag;
            TextView date;
            FancyButton download,share;
        }

        @Override
        public int getCount() {
            return worldpopulationlist.size();
        }

        @Override
        public Object getItem(int position) {
            return worldpopulationlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View view, ViewGroup parent) {
            final ViewHolder holder;
            if (inflater == null) {
                inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.listview_item, null);
                // Locate the ImageView in listview_item.xml
                holder.flag = (SimpleDraweeView) view.findViewById(R.id.flag);
                holder.date = (TextView) view.findViewById(R.id.date);
                holder.download = (FancyButton) view.findViewById(R.id.download);
                holder.share = (FancyButton) view.findViewById(R.id.share);
                holder.play_button = (ImageView) view.findViewById(R.id.play_button);

                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
            }

            if(worldpopulationlist.get(position).getUploadType().equalsIgnoreCase("0"))
            {
                Uri uri = Uri.parse(worldpopulationlist.get(position).getFlag());
                GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(activity.getResources());
                GenericDraweeHierarchy hierarchy = builder
                        .setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
                        .build();
                holder.flag.setImageURI(uri);
                holder.flag.setHierarchy(hierarchy);

                final Dialog dialogZoom = new Dialog(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);

                dialogZoom.setContentView(R.layout.image_dialog);

                final ImageView img = (ImageView) dialogZoom.findViewById(R.id.dialog_image);
                Picasso.with(activity).load(worldpopulationlist.get(position).getFlag()).into(img);

                holder.flag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogZoom.show();
                    }
                });

                img.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        mAttacher = new PhotoViewAttacher(img);
                        return false;
                    }
                });

                holder.download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try{
                            new DownloadFileFromURL().execute(worldpopulationlist.get(position).getFlag(),"a"+worldpopulationlist.get(position).getId());
                        }catch (Exception je)
                        {

                        }
                    }
                });

                holder.date.setText(worldpopulationlist.get(position).getDate());
                holder.play_button.setVisibility(View.GONE);
                holder.share.setVisibility(View.VISIBLE);
                holder.download.setVisibility(View.VISIBLE);

                holder.share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // Create ad request.
                        AdRequest adRequest = new AdRequest.Builder().build();

                        interstitial.loadAd(adRequest);
                        interstitial.setAdListener(new AdListener() {
                            public void onAdLoaded() {
                                interstitial.show();
                            }
                        });

                        interstitial.setAdListener(new AdListener() {
                            public void onAdLoaded() {
                                interstitial.show();
                            }
                        });
                        new ShareFromURL().execute(worldpopulationlist.get(position).getFlag(),"a"+worldpopulationlist.get(position).getId());
                    }
                });
            }
            else if(worldpopulationlist.get(position).getUploadType().equalsIgnoreCase("1"))
            {
                holder.play_button.setVisibility(View.VISIBLE);
                Uri uri = Uri.parse("http://img.youtube.com/vi/" + worldpopulationlist.get(position).getYoutubeID() + "/0.jpg");
                GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(activity.getResources());
                GenericDraweeHierarchy hierarchy = builder
                        .setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
                        .build();
                holder.flag.setImageURI(uri);
                holder.flag.setHierarchy(hierarchy);
                holder.download.setVisibility(View.GONE);
                holder.share.setVisibility(View.GONE);

                holder.flag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.putExtra("youtubeVideoId", worldpopulationlist.get(position).getYoutubeID());
                        intent.putExtra("time", worldpopulationlist.get(position).getDate());
                        intent.setClass(activity, YoutubeView_layout.class);
                        startActivity(intent);

                        // Create ad request.
                        AdRequest adRequest = new AdRequest.Builder().build();

                        interstitial.loadAd(adRequest);
                        interstitial.setAdListener(new AdListener() {
                            public void onAdLoaded() {
                                interstitial.show();
                            }
                        });
                    }
                });
            }

            return view;
        }

        class ShareFromURL extends AsyncTask<String, String, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            /**
             * Downloading file in background thread
             * */
            @Override
            protected String doInBackground(String... f_url) {
                int count;
                File file = null;

                try {
                    URL url = new URL(f_url[0]);
                    URLConnection conection = url.openConnection();
                    conection.connect();
                    // this will be useful so that you can show a tipical 0-100% progress bar
                    int lenghtOfFile = conection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);

                    File mediaStorageDir = null;

                    mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath(), "/The Indian GAG/");

                    // Create the storage directory if it does not exist
                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {
                            return null;
                        }
                    }

//                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
                    String imageName = f_url[1] + ".jpg";
                    file = new File(mediaStorageDir.getPath(), imageName);

                    Log.i("pathreal",file.getPath().toString());

                    if(!file.exists()) {


                        // Output stream
                        OutputStream output = new FileOutputStream(file);

                        byte data[] = new byte[1024];

                        long total = 0;

                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    }
                    else {
                        Log.i("fileexists","file present");
                    }

                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }

                return file.toString();
            }

            @Override
            protected void onPostExecute(String file_url) {

                // Reading image path from sdcard
                String imagePath = file_url;

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");
                Uri imageUri = Uri.parse("file://" + imagePath);
                share.putExtra(Intent.EXTRA_STREAM, imageUri);
                startActivity(Intent.createChooser(share, "Share Image"));

            }

        }


        class DownloadFileFromURL extends AsyncTask<String, String, String> {

            /**
             * Before starting background thread
             * Show Progress Bar Dialog
             * */
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showDialog(progress_bar_type);
            }

            /**
             * Downloading file in background thread
             * */
            @Override
            protected String doInBackground(String... f_url) {
                int count;
                File file = null;

                try {
                    URL url = new URL(f_url[0]);
                    URLConnection conection = url.openConnection();
                    conection.connect();
                    // this will be useful so that you can show a tipical 0-100% progress bar
                    int lenghtOfFile = conection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);

                    File mediaStorageDir = null;

                    mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath(), "/The Indian GAG/");

                    // Create the storage directory if it does not exist
                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {
                            return null;
                        }
                    }

//                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
                    String imageName = f_url[1] + ".jpg";
                    file = new File(mediaStorageDir.getPath(), imageName);

                    Log.i("pathreal",file.getPath().toString());

                    if(!file.exists()) {


                        // Output stream
                        OutputStream output = new FileOutputStream(file);

                        byte data[] = new byte[1024];

                        long total = 0;

                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    }
                    else {
                        Log.i("fileexists","file present");
                    }

                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }

                return file.toString();
            }

            /**
             * Updating progress bar
             * */
            protected void onProgressUpdate(String... progress) {
                // setting progress percentage
                pDialog.setProgress(Integer.parseInt(progress[0]));
            }

            /**
             * After completing background task
             * Dismiss the progress dialog
             * **/
            @Override
            protected void onPostExecute(String file_url) {
                // dismiss the dialog after the file was downloaded
                dismissDialog(progress_bar_type);

                // Displaying downloaded image into image view
                // Reading image path from sdcard
                String imagePath = file_url;

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse("file://" + imagePath), "image/*");
                startActivity(intent);

            }

        }

    }

    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }


}
