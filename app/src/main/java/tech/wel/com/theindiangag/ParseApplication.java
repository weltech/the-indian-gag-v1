package tech.wel.com.theindiangag;


import android.app.Application;
import android.support.multidex.MultiDex;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ParseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Fresco.initialize(this);

        //Parse.initialize(ParseApplication.this, "rqCeGCWp2n69z1gYSQHd2gl4o5uSDm9fR8ZTDb5f", "zkxMayTTL4zIFkt0fSQqDfCb4IEoGdAFYgfCBEut");

        Parse.initialize(new Parse.Configuration.Builder(ParseApplication.this)
                .applicationId("rqCeGCWp2n69z1gYSQHd2gl4o5uSDm9fR8ZTDb5f")
                .clientKey("zkxMayTTL4zIFkt0fSQqDfCb4IEoGdAFYgfCBEut")
                .server("https://parseapi.back4app.com")

        .build()
        );

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("NotoSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // Update Installation
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("GCMSenderId", "949402468565");
        installation.saveInBackground();

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

}