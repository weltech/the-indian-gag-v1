package tech.wel.com.theindiangag;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

/**
 * Created by Administrator on 11/29/2015.
 */
public class CustomPushReceiver extends ParsePushBroadcastReceiver {
    private final String TAG = CustomPushReceiver.class.getSimpleName();


    private Intent parseIntent;

    public CustomPushReceiver() {
        super();
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        if (intent == null)
            return;

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            Log.e(TAG, "Push received: " + json);

            parseIntent = intent;

            parsePushJson(context, json);

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    /**
     * Parses the push notification json
     *
     * @param context
     * @param json
     */
    private void parsePushJson(Context context, JSONObject json) {
        try {
            JSONObject data = json.getJSONObject("data");
            String title = data.getString("title");
            String message = data.getString("message");

                showNotificationMessage(context, title, message);

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    private void showNotificationMessage(Context context, String title, String message) {

        Intent myIntent = null;

        SharedPreferences sp1= context.getSharedPreferences("Login",0);

        String unm = sp1.getString("Unm", null);

        try
        {
            if (unm == null) {
                myIntent = new Intent();
                myIntent.setClass(context, Login.class);
            }
            else {
                myIntent = new Intent();
                myIntent.setClass(context, MainActivity.class);
            }

        }catch (Exception e)
        {

        }

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, myIntent, PendingIntent.FLAG_ONE_SHOT);

//        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.notificon);
        int color = context.getResources().getColor(R.color.notification_background_light);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(getSmallIconId(context,myIntent))
                .setLargeIcon(getLargeIcon(context,myIntent))
                .setColor(color)
                .setContentTitle("The Indian GAG")
                .setContentText(message)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //	notification.number = Integer.valueOf(notCount);
        Random random = new Random();
        int id = random.nextInt(9999 - 1000) + 1000;
        notificationManager.notify(id, notificationBuilder.build());


    }

    @Override
    protected int getSmallIconId(Context context, Intent intent) {
        return R.drawable.ic_stat_tig;
    }

    @Override
    protected Bitmap getLargeIcon(Context context, Intent intent) {
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.notificon);
    }
}